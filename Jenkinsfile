pipeline {
  agent any
  tools {
    maven 'maven3'
    jdk 'jdk8'
  }
  options {
    timestamps()
    timeout(time: 5, unit: 'MINUTES')
  }

  environment {
      REGISTRY="http://172.104.92.240:5000"
      REGISTRY_ID=credentials('tekesi_registry')
  }
  stages {
    stage('Switch Branch') {
      steps {
        sh '''
          git checkout master
          '''
      }
    }

    stage('Code Analysis') {
      steps {
        withSonarQubeEnv('SonarCloud') {
          sh '''
            mvn compile
            mvn sonar:sonar \\
            -Dsonar.projectKey=aka.tekesi:syrups-registry \\
            -Dsonar.organization=tekesi \\
            -Dsonar.host.url=https://sonarcloud.io \\
            -Dsonar.java.binaries=target/classes
            '''
        }
      }
    }
    stage("Quality Gate") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          waitForQualityGate abortPipeline: true
        }
      }
    }
    stage('Package') {
      steps {
        sh 'mvn package'
      }
    }
    stage('Deploy') {
      steps {
        sh 'mvn clean deploy -DskipTests'
      }
    }

    stage('Release') {
      when {
        branch 'master'  //only run these steps on the master branch
      }
      input {
        message "Release this version?"
        ok "OK"
        submitter "nxzh"
        parameters { booleanParam(name: 'RELEASE_FLAG', defaultValue: true, description: '') }
      }
      steps {
        script {
          if (RELEASE_FLAG) {
            sh 'mvn release:clean release:prepare'
            sh 'mvn release:perform'

            TEAM=readMavenPom().getGroupId()
            IMAGE=readMavenPom().getArtifactId()
            VERSION=readMavenPom().getVersion()
            PACKAGING=readMavenPom().getPackaging()

            sh """
              docker build --build-arg ARTIFACT_FILE=${ARTIFACT_FILE} -t ${TEAM}/${IMAGE}:latest .
              docker tag ${TEAM}/${IMAGE}:latest ${REGISTRY}/${TEAM}/${IMAGE}:${VERSION}
              docker tag ${TEAM}/${IMAGE}:latest ${REGISTRY}/${TEAM}/${IMAGE}:latest
              docker login -u ${REGISTRY_ID_USR} -p ${REGISTRY_ID_PSW} ${REGISTRY}
              docker push ${REGISTRY}/${TEAM}/${IMAGE}:${VERSION}
              docker push ${REGISTRY}/${TEAM}/${IMAGE}:latest
            """
          }
        }


      }
    }
  }
  //post {
    //always {
      //deleteDir()
    //}
  //}
}