FROM openjdk:8-jre-alpine
MAINTAINER Sid Zhang <zhangnaixiao@outlook.com>

ARG ARTIFACT_FILE

ENV ARTIFACT_FILE=${ARTIFACT_FILE}

ENV JAVA_OPTS="-Xms64m -Xmx64m"

EXPOSE 8081



RUN mkdir -p /usr/share/tekesi/

VOLUME /usr/share/tekesi

ADD target/${ARTIFACT_FILE} /usr/share/tekesi/${ARTIFACT_FILE}

ENTRYPOINT exec java ${JAVA_OPTS} -jar /usr/share/tekesi/${ARTIFACT_FILE}
